const path = require('path');
const fs = require('fs');

const Ticket = require('./ticket');


class TicketControl{
    constructor(){
        this.ultimo     = 0;
        this.hoy        = new Date().getDate();
        this.tickets    = [] ;
        this.ultimos4   = [];

        this.init();

        console.log(this.ultimos4);
        console.log(this.tickets);
    }

    get toJson(){
        return{
            ultimo:     this.ultimo,
            hoy:        this.hoy,
            tickets:    this.tickets,
            ultimos4:   this.ultimos4
        }
    }

    init(){
        const data = require('../db/data.json');

        if (data.hoy === this.hoy) {
            this.tickets    = data.tickets;
            this.ultimo     = data.ultimo;
            this.ultimos4   = data.ultimos4;
        }else{
            this.guardarDB();
        }
    }

    guardarDB(){
        const dbPath = path.join(__dirname, '../db/data.json' );
        fs.writeFileSync( dbPath, JSON.stringify( this.toJson ) );
    }

    siguiente(){
        this.ultimo += 1;
        const ticket = new Ticket( this.ultimo, null );
        this.tickets.push( ticket );

        this.guardarDB();
        return `Ticket: ${ticket.numero}`;
    }

    atenderTicket( escritorio ){
        if (this.tickets.length === 0) { // No tenemos tickets
            return null
        }

        const ticket = this.tickets.shift();

        ticket.escritorio = escritorio;
        console.log(ticket);

        this.ultimos4.unshift( ticket );
        const ticketAtender = this.ultimos4.pop();

        this.guardarDB();

        return ticketAtender;
    }

    // atenderTicket( escritorio ){
    //     if (this.tickets.length === 0) { // No tenemos tickets
    //         return null
    //     }

    //     const ticket = this.tickets.shift();

    //     ticket.escritorio = escritorio ;

    //     this.ultimos4.unshift( ticket );

    //     if ( this.ultimos4.length > 4 ) {
    //         this.ultimos4.splice(-1,1);
    //     }

    //     this.guardarDB();

    //     return ticket;
    // }
}

module.exports = TicketControl