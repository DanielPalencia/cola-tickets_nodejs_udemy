// Referencias HTML
const lblNuevoTicket = document.querySelector('#lblNuevoTicket');
const btnCrear = document.querySelector('button');


console.log('Nuevo Ticket HTML');

const socket = io();



socket.on('connect', () => {
    btnCrear.disabled = false;
});

socket.on('ultimo-ticket', (ultimo) => {
    lblNuevoTicket.innerText = `Ticket: ${ultimo}`;
});

socket.on('disconnect', () => {
    btnCrear.disabled = false;
});


btnCrear.addEventListener( 'click', () => {
    socket.emit( 'siguiente-ticket', null, ( ticket ) => {
        lblNuevoTicket.innerText = ticket;
    });

});