// const Ticket = require("../../models/ticket");

const lblEscritorio = document.querySelector('h1');
const btnAtender = document.querySelector('button');
const lblTicket = document.querySelector('small');
const divAlerta = document.querySelector('.alert')

const searchParams =  new URLSearchParams(window.location.search);

if ( !searchParams.has( 'escritorio' ) ) {
    window.location = 'index.html';
    throw new Error('El Escritorio es obligatorio');
}

const escritorio = searchParams.get('escritorio');
lblEscritorio.innerText = escritorio;

divAlerta.style.display = 'none';

const socket = io();



socket.on('connect', () => {
    btnAtender.disabled = false;
});

socket.on('disconnect', () => {
    btnAtender.disabled = true;
});

socket.on('ultimo-ticket', (ultimo) => {

});


btnAtender.addEventListener( 'click', () => {
    socket.emit('atender-ticket', { escritorio }, ( {succes, ticket, msg} ) =>{

        if( !succes ){
            lblTicket.innerText = 'Nadie';
            return divAlerta.style.display = '';
        }

        console.log('no');
        lblTicket.innerText = 'Ticket: ' + ticket.numero;
    });
});